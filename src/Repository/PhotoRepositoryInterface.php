<?php

namespace App\Repository;

use App\Entity\Photo;

interface PhotoRepositoryInterface
{
    /**
     * @param Photo $photo
     */
    public function save(Photo $photo): void;
}