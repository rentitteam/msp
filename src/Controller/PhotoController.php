<?php

namespace App\Controller;

use App\Application\EntityPhotoDtoTransformer;
use App\Application\SavePhotoService;
use App\Application\SavePhotoServiceException;
use App\Application\SavePhotoServiceRequest;
use App\Repository\PhotoRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PhotoController extends AbstractController
{
    /**
     * @param PhotoRepositoryInterface $photoRepository
     * @param EntityPhotoDtoTransformer $dtoTransformer
     * @return Response
     */
    public function index(
        PhotoRepositoryInterface $photoRepository,
        EntityPhotoDtoTransformer $dtoTransformer
    ): Response
    {
        $photoEntities = $photoRepository->findAll();
        $photos = [];
        foreach ($photoEntities as $photoEntity) {
            $photos[] = $dtoTransformer->transform($photoEntity);
        }

        return $this->render('photo/index.html.twig', [
            'photos' => $photos
        ]);
    }

    /**
     * @param Request $request
     * @param SavePhotoService $savePhotoService
     * @return Response
     * @throws \Exception
     */
    public function save(
        Request $request,
        SavePhotoService $savePhotoService
    ): Response
    {
        $serviceRequest = new SavePhotoServiceRequest();
        $serviceRequest->pixabayId = (int)$request->request->get('id', 0);
        try {
            $savePhotoService->execute($serviceRequest);
        } catch (SavePhotoServiceException $exception) {
            //do something or ignore this kind of exception
        } catch (\Exception $exception) {
            //do something
        }
    }
}
