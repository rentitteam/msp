<?php

namespace App\Controller;

use App\Application\SearchPhotosServiceException;
use App\Application\SearchPhotosServiceRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Application\SearchPhotosService;

class HomeController extends AbstractController
{
    /**
     * @param Request $request
     * @param SearchPhotosService $searchPhotosService
     * @return Response
     */
    public function index(
        Request $request,
        SearchPhotosService $searchPhotosService
    ): Response
    {
        try {
            $searchRequest = new SearchPhotosServiceRequest();
            $searchRequest->searchParams = (array)$request->request->get('search', []);
            $result = $searchPhotosService->execute($searchRequest);

            return $this->render('home/index.html.twig', [
                'photos' => $result->photos,
                'expireIn' => $result->secondsToExpire,
                'search' => $searchRequest->searchParams
            ]);
        } catch (SearchPhotosServiceException $exception) {
            //do something
        } catch (\Exception $exception) {
            //do something
        }
    }
}
