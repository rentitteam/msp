<?php

namespace App\Application;

class SearchPhotoServiceResult
{
    public array $photos;
    public string $expireAt;
    public int $secondsToExpire;
}