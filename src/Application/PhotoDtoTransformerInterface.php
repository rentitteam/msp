<?php

namespace App\Application;

use App\Dto\PhotoDto;

interface PhotoDtoTransformerInterface
{
    /**
     * @param array $arrObject
     * @return PhotoDto
     */
    public function transform(array $arrObject): PhotoDto;
}