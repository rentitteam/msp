<?php

namespace App\Application;

use App\Dto\PhotoDto;
use App\Entity\Photo;

class EntityPhotoDtoTransformer
{
    /**
     * @param Photo $photo
     * @return PhotoDto
     */
    public function transform(Photo $photo): PhotoDto
    {
        $photoDto = new PhotoDto();
        $photoDto->pixabayId = $photo->getPixabayId();
        $photoDto->imageWidth = $photo->getImageWidth();
        $photoDto->imageHeight = $photo->getImageHeight();
        $photoDto->type = $photo->getType();
        $photoDto->previewURL = $photo->getThumbnailImagePath();
        $photoDto->largeImageURL = $photo->getLargeImagePath();

        return $photoDto;
    }
}