<?php

namespace App\Application;

use App\Dto\PhotoDto;

class PixabayPhotoDtoTransformer implements PhotoDtoTransformerInterface
{
    /**
     * @param array $arrObject
     * @return PhotoDto
     */
    public function transform(array $arrObject): PhotoDto
    {
        $photoDto = new PhotoDto();
        $photoDto->pixabayId = $arrObject['id'];
        $photoDto->imageWidth = $arrObject['imageWidth'];
        $photoDto->imageHeight = $arrObject['imageHeight'];
        $photoDto->type = $arrObject['type'];
        $photoDto->previewURL = $arrObject['previewURL'];
        $photoDto->largeImageURL = $arrObject['largeImageURL'];

        return $photoDto;
    }
}