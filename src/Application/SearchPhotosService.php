<?php

namespace App\Application;

use App\Infrastructure\PixabayBadRequestException;
use App\Infrastructure\PixabayHttpClientInterface;
use App\Infrastructure\PixabayResponseInterface;
use App\Infrastructure\PixabayRuntimeException;
use App\Infrastructure\PixabaySearchSpecification;
use App\Infrastructure\PixabayTooManyRequestsException;

class SearchPhotosService
{
    private PixabayHttpClientInterface $pixabayHttpClient;
    private PhotoDtoTransformerInterface $photoDtoTransformer;

    /**
     * SearchPhotosService constructor.
     * @param PixabayHttpClientInterface $pixabayHttpClient
     * @param PhotoDtoTransformerInterface $photoDtoTransformer
     */
    public function __construct(
        PixabayHttpClientInterface $pixabayHttpClient,
        PhotoDtoTransformerInterface $photoDtoTransformer
    )
    {
        $this->pixabayHttpClient = $pixabayHttpClient;
        $this->photoDtoTransformer = $photoDtoTransformer;
    }

    /**
     * @param SearchPhotosServiceRequest $request
     * @return SearchPhotoServiceResult
     */
    public function execute(SearchPhotosServiceRequest $request): SearchPhotoServiceResult
    {
        try {
            $specification = new PixabaySearchSpecification($request->searchParams);
            $pixabayResponse = $this->pixabayHttpClient->query($specification);

            return $this->createResponse($pixabayResponse);
        } catch (PixabayTooManyRequestsException $exception) {
            //we can catch this here an do some action or not
        } catch (PixabayBadRequestException $exception) {
            //we can catch this here an do some action or not
        } catch (PixabayRuntimeException $exception) {
            //we can catch this here an do some action or not
        } catch (\Exception $e) {
            //do something
            //throw new SearchPhotosServiceException
        }
    }

    /**
     * @param PixabayResponseInterface $pixabayResponse
     * @return SearchPhotoServiceResult
     */
    private function createResponse(PixabayResponseInterface $pixabayResponse): SearchPhotoServiceResult
    {
        $searchPhotoServiceResult = new SearchPhotoServiceResult();
        $searchPhotoServiceResult->expireAt = ($pixabayResponse->getExpiresAt())->format(\DateTime::RFC3339);
        $expiresIn = (new \DateTime())->diff($pixabayResponse->getExpiresAt());
        $searchPhotoServiceResult->secondsToExpire = $expiresIn->days * 86400 + $expiresIn->h * 3600 + $expiresIn->i * 60 + $expiresIn->s;
        $photos = $pixabayResponse->getHits();
        foreach ($photos as $arrPhoto) {
            $output[] = $this->photoDtoTransformer->transform($arrPhoto);
        }
        $searchPhotoServiceResult->photos = $photos;

        return $searchPhotoServiceResult;
    }
}