<?php

namespace App\Application;

use App\Entity\Photo;
use App\Entity\PhotoFactoryInterface;
use App\Infrastructure\ImageCreatorService;
use App\Infrastructure\PixabayBadRequestException;
use App\Infrastructure\PixabayHttpClientInterface;
use App\Infrastructure\PixabayRuntimeException;
use App\Infrastructure\PixabaySearchSpecification;
use App\Infrastructure\PixabayTooManyRequestsException;
use App\Repository\PhotoRepositoryInterface;

class SavePhotoService
{
    private PixabayHttpClientInterface $pixabayHttpClient;
    private PhotoFactoryInterface $photoFactory;
    private PhotoRepositoryInterface $photoRepositoryInterface;
    private ImageCreatorService $imageHandling;

    /**
     * SavePhotoService constructor.
     * @param PixabayHttpClientInterface $pixabayHttpClient
     * @param PhotoFactoryInterface $photoFactory
     * @param PhotoRepositoryInterface $photoRepositoryInterface
     * @param ImageCreatorService $imageHandling
     */
    public function __construct(
        PixabayHttpClientInterface $pixabayHttpClient,
        PhotoFactoryInterface $photoFactory,
        PhotoRepositoryInterface $photoRepositoryInterface,
        ImageCreatorService $imageHandling
    )
    {
        $this->pixabayHttpClient = $pixabayHttpClient;
        $this->photoFactory = $photoFactory;
        $this->photoRepositoryInterface = $photoRepositoryInterface;
        $this->imageHandling = $imageHandling;
    }

    /**
     * @param SavePhotoServiceRequest $request
     */
    public function execute(SavePhotoServiceRequest $request): void
    {
        try {
            $pixabayPhotoArr = $this->getPixabayPhotoById($request->pixabayId);
            if (empty($pixabayPhotoArr) || empty($pixabayPhotoArr['hits'])) {
                throw new \InvalidArgumentException('Photo does not exist');
            }
            $photoEntity = $this->makePhotoEntity($pixabayPhotoArr);

            $this->imageHandling->create($photoEntity);
            $this->photoRepositoryInterface->save($photoEntity);
        } catch (PixabayTooManyRequestsException $exception) {
            //we can catch this here and do some action or not
        } catch (PixabayBadRequestException $exception) {
            //we can catch this here and do some action or not
        } catch (PixabayRuntimeException $exception) {
            //we can catch this here and do some action or not
        } catch (\Exception $e) {
            //or some specific exception
            //rollback images creation etc
            //throw new SavePhotoServiceException
        }
    }

    /**
     * @param int $pixabayId
     * @return array
     */
    private function getPixabayPhotoById(int $pixabayId): array
    {
        $specification = new PixabaySearchSpecification(['id' => $pixabayId]);
        return $this->pixabayHttpClient->query($specification);
    }

    /**
     * @param array $pixabayPhotoArr
     * @return Photo
     */
    private function makePhotoEntity(array $pixabayPhotoArr): Photo
    {
        $pixabayPhoto = array_shift($pixabayPhotoArr['hits']);

        return $this->photoFactory->build(
            $pixabayPhoto['id'],
            $pixabayPhoto['type'],
            $pixabayPhoto['imageWidth'],
            $pixabayPhoto['imageHeight'],
            $pixabayPhoto['previewURL'],
            $pixabayPhoto['largeImageURL']
        );
    }
}