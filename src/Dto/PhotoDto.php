<?php

namespace App\Dto;

class PhotoDto
{
    public $pixabayId;
    public $type;
    public $previewURL;
    public $largeImageURL;
    public $imageWidth;
    public $imageHeight;
    public $imageSize;
}