<?php

namespace App\Entity;


class PhotoFactory implements PhotoFactoryInterface
{
    /**
     * @param int $pixabayId
     * @param string $type
     * @param int $imageWidth
     * @param int $imageHeight
     * @param string $previewURL
     * @param string $largeImageURL
     * @return Photo
     */
    public function build(
        int $pixabayId,
        string $type,
        int $imageWidth,
        int $imageHeight,
        string $previewURL,
        string $largeImageURL
    ): Photo
    {
        $photo = new Photo();
        $photo->setPixabayId($pixabayId)
            ->setType($type)
            ->setImageWidth($imageWidth)
            ->setImageWidth($imageHeight)
            ->setPreviewURL($previewURL)
            ->setLargeImageURL($largeImageURL);

        return $photo;
    }
}