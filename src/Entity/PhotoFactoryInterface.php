<?php

namespace App\Entity;

interface PhotoFactoryInterface
{
    /**
     * @param int $pixabayId
     * @param string $type
     * @param int $imageWidth
     * @param int $imageHeight
     * @param string $previewURL
     * @param string $largeImageURL
     * @return Photo
     */
    public function build(
        int $pixabayId,
        string $type,
        int $imageWidth,
        int $imageHeight,
        string $previewURL,
        string $largeImageURL
    ): Photo;
}