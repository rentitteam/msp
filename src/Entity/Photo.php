<?php

namespace App\Entity;

use App\Repository\PhotoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PhotoRepository::class)
 * @ORM\Table(name="photo")
 */
class Photo
{
    private CONST UPLOAD_DIR = 'upload';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $pixabayId;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $imageWidth;

    /**
     * @ORM\Column(type="integer")
     */
    private $imageHeight;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $previewURL;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $largeImageURL;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPixabayId(): ?int
    {
        return $this->pixabayId;
    }

    public function setPixabayId(int $pixabayId): self
    {
        $this->pixabayId = $pixabayId;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getImageWidth(): ?int
    {
        return $this->imageWidth;
    }

    public function setImageWidth(int $imageWidth): self
    {
        $this->imageWidth = $imageWidth;

        return $this;
    }

    public function getImageHeight(): ?int
    {
        return $this->imageHeight;
    }

    public function setImageHeight(int $imageHeight): self
    {
        $this->imageHeight = $imageHeight;

        return $this;
    }

    public function getPreviewURL(): ?string
    {
        return $this->previewURL;
    }

    public function setPreviewURL(string $previewURL): self
    {
        $this->previewURL = $previewURL;

        return $this;
    }

    public function getLargeImageURL(): ?string
    {
        return $this->largeImageURL;
    }

    public function setLargeImageURL(string $largeImageURL): self
    {
        $this->largeImageURL = $largeImageURL;

        return $this;
    }

    /**
     * @return string
     */
    public function getLargeImagePath(): string
    {
        return sprintf('%s/%d_large.jpg', SELF::UPLOAD_DIR, $this->pixabayId);
    }

    /**
     * @return string
     */
    public function getThumbnailImagePath(): string
    {
        return sprintf('%s/%d_thumb.jpg', SELF::UPLOAD_DIR, $this->pixabayId);
    }
}
