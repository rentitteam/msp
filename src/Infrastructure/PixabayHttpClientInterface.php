<?php

namespace App\Infrastructure;


interface PixabayHttpClientInterface
{
    /**
     * @param SearchSpecificationInterface $specification
     * @return PixabayResponseInterface
     */
    public function query(SearchSpecificationInterface $specification): PixabayResponseInterface;
}