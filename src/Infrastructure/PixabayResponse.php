<?php

namespace App\Infrastructure;


class PixabayResponse implements PixabayResponseInterface
{
    private int $total;
    private int $totalHits;
    private array $hits;
    private \DateTime $expiresAt;

    /**
     * PixabayResponse constructor.
     * @param int $total
     * @param int $totalHits
     * @param array $hits
     * @param \DateTime $expiresAt
     */
    public function __construct(int $total, int $totalHits, array $hits, \DateTime $expiresAt)
    {
        $this->total = $total;
        $this->totalHits = $totalHits;
        $this->hits = $hits;
        $this->expiresAt = $expiresAt;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return int
     */
    public function getTotalHits(): int
    {
        return $this->totalHits;
    }

    /**
     * @return array
     */
    public function getHits(): array
    {
        return $this->hits;
    }

    /**
     * @return \DateTime
     */
    public function getExpiresAt(): \DateTime
    {
        return $this->expiresAt;
    }
}