<?php

namespace App\Infrastructure;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class PixabayHttpClient implements PixabayHttpClientInterface
{
    private HttpClientInterface $httpClient;
    private string $pixabayApiKey;

    /**
     * PixabayHttpClient constructor.
     * @param HttpClientInterface $pixabay_api
     */
    public function __construct(HttpClientInterface $pixabay_api, string $pixabayApiKey)
    {
        $this->httpClient = $pixabay_api;
        $this->pixabayApiKey = $pixabayApiKey;
    }

    /**
     * @param SearchSpecificationInterface $specification
     * @return array
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     *
     */
    public function query(SearchSpecificationInterface $specification): PixabayResponseInterface
    {
        $response = $this->httpClient->request('GET', sprintf('/api/?key=%s&%s', $this->pixabayApiKey, $specification->queryfy()));
        $statusCode = $response->getStatusCode();
        if (429 == $statusCode) {
            throw new PixabayTooManyRequestsException($response->getContent(false), $statusCode);
        }
        if (400 == $statusCode) {
            throw new PixabayBadRequestException($response->getContent(false), $statusCode);
        }
        if (200 != $statusCode) {
            throw new PixabayRuntimeException($response->getContent(false), $statusCode);
        }
        if (!$this->isValidHeaderType($response)) {
            throw new PixabayInvalidHeaderTypeException("Invalid header type returned");
        }

        return $this->createResponse($response->toArray());
    }

    /**
     * @param $response
     * @return bool
     */
    private function isValidHeaderType($response): bool
    {
        return substr_count($response->getHeaders()['content-type'][0], 'json');
    }

    /**
     * @param array $response
     * @return PixabayResponseInterface
     */
    private function createResponse(array $response): PixabayResponseInterface
    {
        return new PixabayResponse(
            $response['total'] ?? 0,
            $response['totalHits'] ?? 0,
            empty($response['hits']) ? [] : $response['hits'],
            new \DateTime()
        );
    }
}