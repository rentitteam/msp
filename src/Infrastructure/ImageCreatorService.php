<?php

namespace App\Infrastructure;

use App\Entity\Photo;
use Gregwar\ImageBundle\Services\ImageHandling;

class ImageCreatorService
{
    private CONST THUMB_WIDTH = 150;
    private CONST THUMB_HEIGHT = 150;

    private ImageHandling $imageHandling;
    private string $projectDir;

    /**
     * ImageCreatorService constructor.
     * @param ImageHandling $imageHandling
     * @param string $projectDir
     */
    public function __construct(ImageHandling $imageHandling, string $projectDir)
    {
        $this->imageHandling = $imageHandling;
        $this->projectDir = $projectDir;
    }

    /**
     * @param Photo $photo
     * @throws \Exception
     */
    public function create(Photo $photo)
    {
        $this->createLargeImage($photo);
        $this->createThumbnailImage($photo);
    }

    /**
     * @param Photo $photo
     * @return void
     */
    private function createLargeImage(Photo $photo): void
    {
        $data = file_get_contents($photo->getLargeImageURL());
        $image = $this->buildLargeImagePath($photo);
        file_put_contents($image, $data);
    }

    /**
     * @param Photo $photo
     * @throws \Exception
     */
    private function createThumbnailImage(Photo $photo): void
    {
        $image = $this->buildLargeImagePath($photo);
        $thumbnail = $this->buildThumbnailImagePath($photo);
        $this->imageHandling->open($image)
            ->resize(SELF::THUMB_WIDTH, SELF::THUMB_HEIGHT)
            ->save($thumbnail);
    }

    /**
     * @param Photo $photo
     * @return string
     */
    private function buildLargeImagePath(Photo $photo): string
    {
        return sprintf('%s/%s', $this->projectDir, $photo->getLargeImagePath());
    }

    /**
     * @param Photo $photo
     * @return string
     */
    private function buildThumbnailImagePath(Photo $photo): string
    {
        return sprintf('%s/%s', $this->projectDir, $photo->getThumbnailImagePath());
    }
}