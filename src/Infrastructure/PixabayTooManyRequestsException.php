<?php

namespace App\Infrastructure;


class PixabayTooManyRequestsException extends \RuntimeException {}