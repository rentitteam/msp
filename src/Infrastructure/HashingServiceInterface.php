<?php

namespace App\Infrastructure;

interface HashingServiceInterface
{
    /**
     * @param string $string
     * @return string
     */
    public function hash(string $string): string;
}