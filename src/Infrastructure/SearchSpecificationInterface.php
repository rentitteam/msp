<?php

namespace App\Infrastructure;


interface SearchSpecificationInterface
{
    /**
     * @return string
     */
    public function queryfy(): string;
}