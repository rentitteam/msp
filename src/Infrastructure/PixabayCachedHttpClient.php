<?php

namespace App\Infrastructure;

use Psr\Cache\CacheItemPoolInterface;

class PixabayCachedHttpClient implements PixabayHttpClientInterface
{
    private PixabayHttpClientInterface $pixabayHttpClient;
    private CacheItemPoolInterface $cachePool;
    private HashingServiceInterface $hashService;

    /**
     * CachedPixabayHttpClient constructor.
     * @param PixabayHttpClientInterface $pixabayHttpClient
     * @param CacheItemPoolInterface $cachePool
     * @param HashingServiceInterface $hashService
     */
    public function __construct(
        PixabayHttpClientInterface $pixabayHttpClient,
        CacheItemPoolInterface $cachePool,
        HashingServiceInterface $hashService
    )
    {
        $this->pixabayHttpClient = $pixabayHttpClient;
        $this->cachePool = $cachePool;
        $this->hashService = $hashService;
    }

    /**
     * @param SearchSpecificationInterface $specification
     * @return array
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function query(SearchSpecificationInterface $specification): PixabayResponseInterface
    {
        $cache = $this->cachePool->getItem(
            $this->hashService->hash(
                $specification->queryfy()
            )
        );

        if ($cache->isHit()) {
            return $cache->get();
        }

        $apiResponse = $this->pixabayHttpClient->query($specification);
        $cacheResponse = new Pixabay24hResponse($apiResponse);

        $cache->set($cacheResponse);
        $cache->expiresAt($cacheResponse->getExpiresAt());
        $this->cachePool->save($cache);

        return $apiResponse;
    }
}