<?php

namespace App\Infrastructure;


class Sha1HashingService implements HashingServiceInterface
{
    /**
     * @param string $string
     * @return string
     */
    public function hash(string $string): string
    {
        return sha1($string);
    }
}