<?php

namespace App\Infrastructure;

class PixabaySearchSpecification implements SearchSpecificationInterface
{
    private $optionsList = [
        'response_group',
        'id',
        'q',
        'lang',
        'image_type',
        'orientation',
        'category',
        'min_width',
        'min_height',
        'page',
        'per_page'
    ];

    /** @var array */
    private $params;

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    public function queryfy(): string
    {
        $filtered = array_filter($this->params, function($key) {
            return in_array($key, $this->optionsList);
        }, ARRAY_FILTER_USE_KEY);

        return http_build_query($filtered);
    }
}