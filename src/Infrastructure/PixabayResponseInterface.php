<?php


namespace App\Infrastructure;


interface PixabayResponseInterface
{
    /**
     * @return int
     */
    public function getTotal(): int;

    /**
     * @return int
     */
    public function getTotalHits(): int;

    /**
     * @return array
     */
    public function getHits(): array;

    /**
     * @return \DateTime
     */
    public function getExpiresAt(): \DateTime;
}