<?php

namespace App\Infrastructure;


class Pixabay24hResponse implements PixabayResponseInterface
{
    private const CACHE_TTL = '+1 day';
    private PixabayResponseInterface $pixabayResponse;

    /**
     * PixabayCached1dResponse constructor.
     * @param PixabayResponseInterface $pixabayResponse
     */
    public function __construct(PixabayResponseInterface $pixabayResponse)
    {
        $this->pixabayResponse = $pixabayResponse;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->pixabayResponse->getTotal();
    }

    /**
     * @return int
     */
    public function getTotalHits(): int
    {
        return $this->pixabayResponse->getTotalHits();
    }

    /**
     * @return array
     */
    public function getHits(): array
    {
        return $this->pixabayResponse->getHits();
    }

    /**
     * @return \DateTime
     */
    public function getExpiresAt(): \DateTime
    {
        return (clone $this->pixabayResponse->getExpiresAt())->add(\DateInterval::createFromDateString(self::CACHE_TTL));
    }
}