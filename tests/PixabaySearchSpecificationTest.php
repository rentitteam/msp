<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Infrastructure\PixabaySearchSpecification;

class PixabaySearchSpecificationTest extends TestCase
{
    public function test_filterOutInvalidParameters(): void
    {
        $input = [
            'invalid1' => '1',
            'invalid2' => '2',
            'invalid3' => '3',
            'q' => 'someString',
            'min_width' => 20
        ];

        $specification = new PixabaySearchSpecification($input);
        $query = $specification->queryfy();
        $this->assertStringNotContainsString('invalid1=1', $query);
        $this->assertStringNotContainsString('invalid2=2', $query);
        $this->assertStringNotContainsString('invalid3=3', $query);
        $this->assertStringContainsString('q=someString', $query);
        $this->assertStringContainsString('min_width=20', $query);
    }

    public function test_urlIsEncoded()
    {
        $input = [
            'q' => 'yellow flower'
        ];
        $specification = new PixabaySearchSpecification($input);
        $query = $specification->queryfy();
        $this->assertStringContainsString('q=yellow+flower', $query);
    }
}
